FROM node:12.13.1-alpine

ARG NODE_ENV="production"

ENV NODE_ENV=${NODE_ENV}

RUN mkdir /app

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --production

# copy code
COPY . .

EXPOSE 7001
## Launch the wait tool and then your application
CMD ["npm", "run", "start"]