module.exports = app => {
  const { router, controller, middleware } = app
  const { admin, api } = controller
  const auth = middleware.auth()

  // Admin APIs
  // - User
  router.post('/admin/users/addUser', admin.user.addUser)
  router.post('/admin/users/getUser', admin.user.getUser)
  router.post('/admin/users/getUsers', admin.user.getUsers)
  router.post('/admin/users/modifyUser', admin.user.modifyUser)
  router.post('/admin/users/removeUser', admin.user.removeUser)

  // - Article
  router.post('/admin/articles/addArticle', admin.article.addArticle)
  router.post('/admin/articles/getArticle', admin.article.getArticle)
  router.post('/admin/articles/getArticles', admin.article.getArticles)
  router.post('/admin/articles/modifyArticle', admin.article.modifyArticle)
  router.post('/admin/articles/removeArticle', admin.article.removeArticle)

  // - Pet
  router.post('/admin/pets/addPet', admin.pet.addPet)
  router.post('/admin/pets/getPet', admin.pet.getPet)
  router.post('/admin/pets/getPets', admin.pet.getPets)
  router.post('/admin/pets/modifyPet', admin.pet.modifyPet)
  router.post('/admin/pets/removePet', admin.pet.removePet)

  // - Product
  router.post('/admin/products/addProduct', admin.product.addProduct)
  router.post('/admin/products/getProduct', admin.product.getProduct)
  router.post('/admin/products/getProducts', admin.product.getProducts)
  router.post('/admin/products/modifyProduct', admin.product.modifyProduct)
  router.post('/admin/products/removeProduct', admin.product.removeProduct)

  // - OwnedProduct
  router.post('/admin/ownedProducts/addOwnedProduct', admin.ownedProduct.addOwnedProduct)
  router.post('/admin/ownedProducts/getOwnedProduct', admin.ownedProduct.getOwnedProduct)
  router.post('/admin/ownedProducts/getOwnedProducts', admin.ownedProduct.getOwnedProducts)
  router.post('/admin/ownedProducts/modifyOwnedProduct', admin.ownedProduct.modifyOwnedProduct)
  router.post('/admin/ownedProducts/removeOwnedProduct', admin.ownedProduct.removeOwnedProduct)

  // - Category
  router.post('/admin/categories/addCategory', admin.category.addCategory)
  router.post('/admin/categories/getCategory', admin.category.getCategory)
  router.post('/admin/categories/getCategories', admin.category.getCategories)
  router.post('/admin/categories/modifyCategory', admin.category.modifyCategory)
  router.post('/admin/categories/removeCategory', admin.category.removeCategory)

  // - message
  router.post('/admin/messages/addMessage', admin.message.addMessage)
  router.post('/admin/messages/getMessage', admin.message.getMessage)
  router.post('/admin/messages/getMessages', admin.message.getMessages)
  router.post('/admin/messages/modifyMessage', admin.message.modifyMessage)
  router.post('/admin/messages/removeMessage', admin.message.removeMessage)
  router.post('/admin/messages/removeMessages', admin.message.removeMessages)

  // Outer APIs
  // - User
  router.post('/users/signInSignUp', api.user.SignInSignUp)
  router.post('/users/getCurrentUser', auth, api.user.getCurrentUser)
  router.post('/users/modifyCurrentUser', auth, api.user.modifyCurrentUser)

  // - Article
  router.post('/articles/addCurrentArticle', auth, api.article.addCurrentArticle)
  router.post('/articles/getCurrentArticle', auth, api.article.getCurrentArticle)
  router.post('/articles/getCurrentArticles', auth, api.article.getCurrentArticles)
  router.post('/articles/getCurrentPosts', auth, api.article.getCurrentPosts)
  router.post('/articles/modifyCurrentArticle', auth, api.article.modifyCurrentArticle)
  router.post('/articles/removeCurrentArticle', auth, api.article.removeCurrentArticle)

  // - Pet
  router.post('/pets/getCurrentPet', auth, api.pet.getCurrentPet)
  router.post('/pets/modifyCurrentPet', auth, api.pet.modifyCurrentPet)

  // - Product
  router.post('/products/getProducts', auth, api.product.getProducts)
  router.post('/products/buyProduct', auth, api.product.buyProduct)

  // - OwnedProduct
  router.post('/ownedProducts/getOwnedProducts', auth, api.ownedProduct.getOwnedProducts)
  router.post('/ownedProducts/useOwnedProduct', auth, api.ownedProduct.useOwnedProduct)

  // - Category
  router.post('/categories/addCurrentCategory', auth, api.category.addCurrentCategory)
  router.post('/categories/getCurrentCategory', auth, api.category.getCurrentCategory)
  router.post('/categories/getCurrentCategories', auth, api.category.getCurrentCategories)
  router.post('/categories/modifyCurrentCategory', auth, api.category.modifyCurrentCategory)
  router.post('/categories/removeCurrentCategory', auth, api.category.removeCurrentCategory)

  // - Message
  router.post('/message/addCurrentMessage', auth, api.message.addCurrentMessage)
  router.post('/message/getCurrentMessages', auth, api.message.getCurrentMessages)
}
