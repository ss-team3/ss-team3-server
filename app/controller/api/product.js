const { Controller } = require('egg')
const validateType = require('../../../lib/validate')

class ProductApiController extends Controller {
  async getProducts () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order = { type: 1 } } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.product.findAll({
      filter,
      limit,
      offset,
      order,
    })
    ctx.body = { total, data }
    logger.info('getProducts success')
  }

  async buyProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body
    const { uid } = ctx.auth

    ctx.validate(validateType._id, { _id })

    await service.product.buyOne({ user_id: uid, product_id: _id })

    ctx.body = { success: true }
    logger.info('buyProduct success')
  }
}

module.exports = ProductApiController
