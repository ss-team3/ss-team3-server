const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')
const { extract } = require('article-parser')

class ArticleApiController extends Controller {
  async addCurrentArticle () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body
    const { url } = params

    ctx.validate(validateType.addCurrentArticle, params)

    try {
      const data = await extract(url)
      const image = data.image === '' ? 'https://picsum.photos/300/200' : data.image
      const ttr = Math.max(1, (data.ttr >= 60 ? (data.ttr / 60) : Math.floor(data.content.length * 0.8 / 1000)))
      Object.assign(params, {
        title: data.title,
        content: data.content,
        estimated_time: ttr,
        cover_url: image,
        date: data.published ? Date.parse(data.published) : null,
        archived: false,
        archived_at: null,
      })
    } catch (err) {
      console.log(err)
      throw new e.ErrorRes(e.ArticleExtractionFail)
    }

    Object.assign(params, { user_id: uid })
    const article = await service.article.create(params)
    ctx.body = article
    logger.info('addCurrentArticle success')
  }

  async getCurrentArticle () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const article = await service.article.findOne({ _id, user_id: uid })
    if (!article) {
      throw new e.ErrorRes(e.ArticleNotFound)
    }
    ctx.body = article
    logger.info('getCurrentArticle success')
  }

  async getCurrentArticles () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.article.findAll({ filter: { user_id: uid, ...filter }, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getCurrentArticles success')
  }

  async getCurrentPosts () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.article.findAllPosts({ filter: { user_id: uid, ...filter }, limit, offset, order })
    await service.article.attachCategories(data, uid)
    ctx.body = { total, data }
    logger.info('getCurrentPosts success')
  }

  async modifyCurrentArticle () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType._id, { _id })
    ctx.validate(validateType.modifyCurrentArticle, params)

    if (params.archived) {
      const article = await service.article.findOne({ _id, user_id: uid })
      if (!article.archived_at) {
        const user = await service.user.findByPK(uid)
        await service.user.updateByPK(uid, { money: user.money + article.estimated_time })
      }
      params.archived_at = Date.now()
    }

    const data = await service.article.updateOne({ _id, user_id: uid }, params)
    if (!data.n) {
      throw new e.ErrorRes(e.ArticleNotFound)
    }
    ctx.body = { success: true }
    logger.info('modifyCurrentArticle success')
  }

  async removeCurrentArticle () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.article.deleteOne({ _id, user_id: uid })
    if (!res.deletedCount) {
      throw new e.ErrorRes(e.ArticleNotFound)
    }
    ctx.body = { success: true }
    logger.info('removeCurrentArticle success')
  }
}

module.exports = ArticleApiController
