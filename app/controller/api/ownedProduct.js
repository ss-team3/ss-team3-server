const { Controller } = require('egg')
const validateType = require('../../../lib/validate')

class OwnedProductApiController extends Controller {
  async getOwnedProducts () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params
    const { uid } = ctx.auth

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.ownedProduct.findAll({
      filter: { user_id: uid, ...filter },
      limit,
      offset,
      order,
    })
    await service.ownedProduct.attachProducts(data)

    ctx.body = { total, data }
    logger.info('getOwnedProducts success')
  }

  async useOwnedProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body
    const { uid } = ctx.auth

    ctx.validate(validateType._id, { _id })

    await service.ownedProduct.useOne({ ownedProduct_id: _id, user_id: uid })

    ctx.body = { success: true }
    logger.info('buyProduct success')
  }
}

module.exports = OwnedProductApiController
