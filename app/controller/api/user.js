const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')
const admin = require('firebase-admin')

class UserApiController extends Controller {
  async getCurrentUser () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth

    ctx.validate(validateType.uid, { uid })

    const user = await service.user.findByPK(uid)
    await service.user.attachWeekAlreadyRead([user])
    if (!user) {
      throw new e.ErrorRes(e.UserNotFound)
    }
    ctx.body = user
    logger.info('getCurrentUser success')
  }

  async modifyCurrentUser () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body

    ctx.validate(validateType.uid, { uid })
    ctx.validate(validateType.modifyCurrentUser, params)

    const res = await service.user.updateByPK(uid, params)
    if (!res.n) {
      throw new e.ErrorRes(e.UserNotFound)
    }
    ctx.body = { success: true }
    logger.info('modifyCurrentUser success')
  }

  async SignInSignUp () {
    const { ctx, service, logger } = this
    const { token } = ctx.request.body
    let uid = null
    let name = ''
    let user = null

    ctx.validate(validateType.token, { token })

    // verify token
    try {
      const decodedToken = await admin.auth().verifyIdToken(token)
      // `decodedToken` contains `uid`, `name`, `email`, `picture`...
      uid = decodedToken.uid
      name = decodedToken.name
    } catch (err) {
      console.error(err)
      throw new e.ErrorRes(e.JwtVerifyError)
    }

    user = await service.user.findByPK(uid)
    // auto signup for new user
    if (!user) {
      user = await service.user.create({
        uid,
        name,
        theme: 'light',
        font_family: 'font-sans',
        font_size: 2,
        target_read_article: 7,
      })
      user = user.toObject()

      // Create default pet
      await service.pet.create({
        user_id: user.uid,
        name: `${user.name}'s pet`,
        type: 'cat',
        hunger: 6,
      })
      // Create default category
      await service.category.create({
        user_id: user.uid,
        name: 'All',
        avatar_url: '',
      })

      logger.info('signUp user successfully')
    } else {
      logger.info('signIn user successfully')
    }
    await service.user.attachWeekAlreadyRead([user])

    ctx.body = { token, ...user }
  }
}

module.exports = UserApiController
