const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class PetApiController extends Controller {
  async getCurrentPet () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth

    const pet = await service.pet.findByUserId(uid)
    const currentTime = Date.now()
    const hungerPoint = Math.floor((currentTime - pet.last_feed_at) / 3600000)
    if (hungerPoint > 0) {
      const newHunger = (pet.hunger - hungerPoint >= 0) ? pet.hunger - hungerPoint : 0
      await service.pet.updateByPK(pet._id, { hunger: newHunger, last_feed_at: currentTime })
      pet.hunger = newHunger
      pet.last_feed_at = currentTime
    }
    if (!pet) {
      throw new e.ErrorRes(e.PetNotFound)
    }
    ctx.body = pet
    logger.info('getCurrentPet success')
  }

  async modifyCurrentPet () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    // Only one cat per user
    const params = ctx.request.body

    ctx.validate(validateType.modifyCurrentPet, params)

    const res = await service.pet.updateByUserId(uid, params)
    if (!res.n) {
      throw new e.ErrorRes(e.PetNotFound)
    }
    ctx.body = { success: true }
    logger.info('modifyCurrentPet success')
  }
}

module.exports = PetApiController
