const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class CategoryApiController extends Controller {
  async addCurrentCategory () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body

    ctx.validate(validateType.addCurrentCategory, params)

    Object.assign(params, { user_id: uid })
    const category = await service.category.create(params)
    ctx.body = category
    logger.info('addCurrentCategory success')
  }

  async getCurrentCategory () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id } = ctx.request.body

    // ctx.validate( validateType.getCurrentCategory , { _id })
    ctx.validate(validateType._id, { _id })

    const category = await service.category.findOne({ _id, user_id: uid })
    if (!category) {
      throw new e.ErrorRes(e.CategoryNotFound)
    }
    ctx.body = category
    logger.info('getCurrentCategory success')
  }

  async getCurrentCategories () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.category.findAll({ filter: { user_id: uid, ...filter }, limit, offset, order })

    // attach articles to every category
    await service.category.attachPosts(data, uid)

    ctx.body = { total, data }
    logger.info('getCurrentCategories success')
  }

  async modifyCurrentCategory () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType._id, { _id })
    ctx.validate(validateType.modifyCurrentCategory, params)

    const res = await service.category.updateOne({ _id, user_id: uid }, params)
    if (!res.n) {
      throw new e.ErrorRes(e.CategoryNotFound)
    }
    ctx.body = { success: true }
    logger.info('modifyCurrentCategory success')
  }

  async removeCurrentCategory () {
    const { ctx, service, logger } = this
    const { uid } = ctx.auth
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const defaultCategory = await service.category.findOne({ user_id: uid, name: 'All' })
    if (!defaultCategory) {
      throw new e.ErrorRes(e.DefaultCategoryNotFound)
    }
    await service.article.updateMany({ user_id: uid, category_id: _id }, { category_id: defaultCategory._id })

    const res = await service.category.deleteOne({ _id, user_id: uid })
    if (!res.deletedCount) {
      throw new e.ErrorRes(e.CategoryNotFound)
    }
    ctx.body = { success: true }
    logger.info('removeCurrentCategory success')
  }
}

module.exports = CategoryApiController
