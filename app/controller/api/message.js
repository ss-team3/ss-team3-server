const { Controller } = require('egg')
const validateType = require('../../../lib/validate')

class MessageApiController extends Controller {
  async addCurrentMessage () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { uid } = ctx.auth

    ctx.validate(validateType.addCurrentMessage, params)

    Object.assign(params, { user_id: uid })
    const message = await service.message.createCurrent(params, uid)
    ctx.body = message
    logger.info('addMessage success')
  }

  async getCurrentMessages () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params
    const { uid } = ctx.auth

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.message.findAll({
      filter: { user_id: uid, ...filter },
      limit,
      offset,
      order,
    })
    await service.message.attachPosts(data, uid)

    ctx.body = { total, data }
    logger.info('getCurrentMessage success')
  }
}

module.exports = MessageApiController
