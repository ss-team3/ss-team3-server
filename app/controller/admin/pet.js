const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class PetAdminController extends Controller {
  async addPet () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addPet, params)

    const pet = await service.pet.create(params)
    ctx.body = pet
    logger.info('addPet success')
  }

  async getPet () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const pet = await service.pet.findByPK(_id)
    if (!pet) {
      throw new e.ErrorRes(e.PetNotFound)
    }
    ctx.body = pet
    logger.info('getPet success')
  }

  async getPets () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.pet.findAll({ filter, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getPets success')
  }

  async modifyPet () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType.modifyPet, { _id, ...params })

    const res = await service.pet.updateByPK(_id, params)
    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyPet success')
  }

  async removePet () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.pet.deleteByPK(_id)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removePet success')
  }
}

module.exports = PetAdminController
