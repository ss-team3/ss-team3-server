const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class ProductAdminController extends Controller {
  async addProduct () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addProduct, params)

    const product = await service.product.create(params)
    ctx.body = product
    logger.info('addProduct success')
  }

  async getProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const product = await service.product.findByPK(_id)
    if (!product) {
      throw new e.ErrorRes(e.ProductNotFound)
    }
    ctx.body = product
    logger.info('getProduct success')
  }

  async getProducts () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.product.findAll({
      filter,
      limit,
      offset,
      order,
    })
    ctx.body = { total, data }
    logger.info('getProducts success')
  }

  async modifyProduct () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType._id, { _id })
    ctx.validate(validateType.modifyProduct, params)

    const res = await service.product.updateByPK(_id, params)

    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyProduct success')
  }

  async removeProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.product.deleteByPK(_id)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeProduct success')
  }
}

module.exports = ProductAdminController
