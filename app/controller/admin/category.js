const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class CategoryAdminController extends Controller {
  async addCategory () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addCategory, params)

    const category = await service.category.create(params)
    ctx.body = category
    logger.info('addCategory success')
  }

  async getCategory () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const category = await service.category.findByPK(_id)
    if (!category) {
      throw new e.ErrorRes(e.CategoryNotFound)
    }
    ctx.body = category
    logger.info('getCategory success')
  }

  async getCategories () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.category.findAll({ filter, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getCategories success')
  }

  async modifyCategory () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType.modifyCategory, { _id, ...params })

    const res = await service.category.updateByPK(_id, params)
    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyCategory success')
  }

  async removeCategory () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.category.deleteByPK(_id)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeCategory success')
  }
}

module.exports = CategoryAdminController
