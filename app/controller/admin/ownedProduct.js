const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class OwnedProductAdminController extends Controller {
  async addOwnedProduct () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addOwnedProduct, params)

    const ownedProduct = await service.ownedProduct.create(params)
    ctx.body = ownedProduct
    logger.info('addOwnedProduct success')
  }

  async getOwnedProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const ownedProduct = await service.ownedProduct.findByPK(_id)
    if (!ownedProduct) {
      throw new e.ErrorRes(e.OwnedProductNotFound)
    }
    ctx.body = ownedProduct
    logger.info('getOwnedProduct success')
  }

  async getOwnedProducts () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.ownedProduct.findAll({
      filter,
      limit,
      offset,
      order,
    })
    ctx.body = { total, data }
    logger.info('getOwnedProducts success')
  }

  async modifyOwnedProduct () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType.modifyOwnedProduct, { _id, ...params })

    const res = await service.ownedProduct.updateByPK(_id, params)

    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyOwnedProduct success')
  }

  async removeOwnedProduct () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.ownedProduct.deleteByPK(_id)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeOwnedProduct success')
  }
}

module.exports = OwnedProductAdminController
