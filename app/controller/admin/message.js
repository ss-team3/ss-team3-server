const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class MessageAdminController extends Controller {
  async addMessage () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addMessage, params)

    const message = await service.message.create(params)
    ctx.body = message
    logger.info('addMessage success')
  }

  async getMessage () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const message = await service.message.findByPK(_id)
    if (!message) {
      throw new e.ErrorRes(e.MessageNotFound)
    }
    ctx.body = message
    logger.info('getMessage success')
  }

  async getMessages () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.message.findAll({ filter, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getMessages success')
  }

  async modifyMessage () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType.modifyMessage, { _id, ...params })

    const res = await service.message.updateByPK(_id, params)
    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyMessage success')
  }

  async removeMessage () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.message.deleteByPK(_id)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeMessage success')
  }

  async removeMessages () {
    const { ctx, service, logger } = this
    const filter = ctx.request.body

    ctx.validate(validateType.filter, { filter })

    const res = await service.message.deleteMany(filter)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeMessages success')
  }
}

module.exports = MessageAdminController
