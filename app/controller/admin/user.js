const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')

class UserAdminController extends Controller {
  async addUser () {
    const { ctx, service, logger } = this
    const params = ctx.request.body

    ctx.validate(validateType.addUser, params)

    const user = await service.user.create(params)
    ctx.body = user
    logger.info('addUser success')
  }

  async getUser () {
    const { ctx, service, logger } = this
    const { filter } = ctx.request.body

    // ctx.validate(validateType.uid, { uid })

    const user = await service.user.findOne(filter)
    if (!user) {
      throw new e.ErrorRes(e.UserNotFound)
    }
    ctx.body = user
    logger.info('getUser success')
  }

  async getUsers () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.user.findAll({ filter, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getUsers success')
  }

  async modifyUser () {
    const { ctx, service, logger } = this
    const { uid, ...params } = ctx.request.body

    ctx.validate(validateType.modifyUser, { uid, ...params })

    const res = await service.user.updateByPK(uid, params)
    ctx.body = { success: true, nModified: res.nModified }
    logger.info('modifyUser success')
  }

  async removeUser () {
    const { ctx, service, logger } = this
    const { uid } = ctx.request.body

    ctx.validate(validateType.uid, { uid })

    const res = await service.user.deleteByPK(uid)
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeUser success')
  }
}

module.exports = UserAdminController
