const { Controller } = require('egg')
const e = require('../../../lib/error')
const validateType = require('../../../lib/validate')
const { extract } = require('article-parser')

class ArticleAdminController extends Controller {
  async addArticle () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { url } = params

    ctx.validate(validateType.addArticle, params)

    try {
      const data = await extract(url)
      const image = (data.image === '') ? 'https://picsum.photos/300/200' : data.image
      Object.assign(params, {
        title: data.title,
        content: data.content,
        estimated_time: Math.floor(data.ttr / 60),
        cover_url: image,
        date: data.published,
      })
    } catch (err) {
      console.log(err)
      throw new e.ErrorRes(e.ArticleExtractionFail)
    }

    const article = await service.article.create(params)
    ctx.body = article
    logger.info('addArticle success')
  }

  async getArticle () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const article = await service.article.findByPK(_id)
    if (!article) {
      throw new e.ErrorRes(e.ArticleNotFound)
    }
    ctx.body = article
    logger.info('getArticle success')
  }

  async getArticles () {
    const { ctx, service, logger } = this
    const params = ctx.request.body
    const { filter, limit, offset, order } = params

    ctx.validate(validateType.gets, params)

    const { total, data } = await service.article.findAll({ filter, limit, offset, order })
    ctx.body = { total, data }
    logger.info('getArticles success')
  }

  async modifyArticle () {
    const { ctx, service, logger } = this
    const { _id, ...params } = ctx.request.body

    ctx.validate(validateType.modifyArticle, { _id, ...params })

    const data = await service.article.updateOne({ _id }, params)
    ctx.body = { success: true, nModified: data.nModified }
    logger.info('modifyArticle success')
  }

  async removeArticle () {
    const { ctx, service, logger } = this
    const { _id } = ctx.request.body

    ctx.validate(validateType._id, { _id })

    const res = await service.article.deleteOne({ _id })
    ctx.body = { success: true, deletedCount: res.deletedCount }
    logger.info('removeArticle success')
  }
}

module.exports = ArticleAdminController
