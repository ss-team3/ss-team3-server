// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const UserSchema = new Schema({
    // use Firebase user uid as primary key
    uid: {
      type: String,
      unique: true,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    theme: {
      type: String,
      required: true,
    },
    font_family: {
      type: String, // font-sans, font-serif, font-mono
      required: true,
    },
    font_size: {
      type: Number, // [0:7] => [xs, sm, base, lg, xl, 2xl, 3xl, 4xl]
      required: true,
    },
    money: {
      type: Number,
      default: 0,
      required: true,
    },
    total_read_time: {
      type: Number,
      default: 0,
      required: true,
    },
    total_read_article: {
      type: Number,
      default: 0,
      required: true,
    },
    target_read_article: {
      type: Number,
      required: true,
    },
    created_at: {
      type: Number,
    },
    updated_at: {
      type: Number,
    },
  }, {
    // ref: https://mongoosejs.com/docs/guide.html#strict
    strict: 'throw',
    // ref: https://mongoosejs.com/docs/guide.html#timestamps
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  })

  UserSchema.index({ uid: 1 }, { unique: true })
  UserSchema.index({ name: 1 })

  return mongoose.model('User', UserSchema)
}
