// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = (app) => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema
  const { ObjectId } = Schema.Types

  const PetSchema = new Schema(
    {
      // Mongoose assigns an `id` virtual getter by default
      // which returns the document's _id field cast to a string
      // ref: https://mongoosejs.com/docs/guide.html#id
      _id: {
        type: ObjectId,
        auto: true,
      },
      user_id: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      type: {
        type: String,
        require: true,
      },
      hunger: {
        type: Number,
        require: true,
      },
      last_feed_at: {
        type: Number,
        require: true,
      },
    },
    {
      // ref: https://mongoosejs.com/docs/guide.html#strict
      strict: 'throw',
    },
  )

  PetSchema.index({ user_id: 1, type: 1 }, { unique: true })

  return mongoose.model('Pet', PetSchema)
}
