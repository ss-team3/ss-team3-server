// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = (app) => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema
  const { ObjectId } = Schema.Types

  const ProductSchema = new Schema(
    {
      // Mongoose assigns an `id` virtual getter by default
      // which returns the document's _id field cast to a string
      // ref: https://mongoosejs.com/docs/guide.html#id
      _id: {
        type: ObjectId,
        auto: true,
      },
      name: {
        type: String,
        required: true,
      },
      price: {
        type: Number,
        require: true,
      },
      energy: {
        type: Number,
        require: true,
      },
      type: {
        type: String,
        require: true,
      },
    },
    {
      // ref: https://mongoosejs.com/docs/guide.html#strict
      strict: 'throw',
    },
  )

  ProductSchema.index({ name: 1 })

  return mongoose.model('Product', ProductSchema)
}
