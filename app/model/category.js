// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
const { ObjectId } = require('mongoose').Types
module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema

  const CategorySchema = new Schema({
    _id: {
      type: ObjectId,
      auto: true,
    },
    user_id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    avatar_url: {
      type: String,
    },
  }, {
    // ref: https://mongoosejs.com/docs/guide.html#strict
    strict: 'throw',
  })

  CategorySchema.index({ user_id: 1, name: 1 }, { unique: true })

  return mongoose.model('Category', CategorySchema)
}
