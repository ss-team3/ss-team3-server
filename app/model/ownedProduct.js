// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = (app) => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema
  const { ObjectId } = Schema.Types

  const OwnedProductSchema = new Schema(
    {
      // Mongoose assigns an `id` virtual getter by default
      // which returns the document's _id field cast to a string
      // ref: https://mongoosejs.com/docs/guide.html#id
      _id: {
        type: ObjectId,
        auto: true,
      },
      user_id: {
        type: String,
        required: true,
      },
      product_id: {
        type: String,
        required: true,
      },
      count: {
        type: Number,
        required: true,
      },
    },
    {
      // ref: https://mongoosejs.com/docs/guide.html#strict
      strict: 'throw',
    },
  )

  OwnedProductSchema.index({ user_id: 1, product_id: 1 }, { unique: true })

  return mongoose.model('OwnedProduct', OwnedProductSchema)
}
