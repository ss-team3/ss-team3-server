// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = app => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema
  const { ObjectId } = Schema.Types

  const ArticleSchema = new Schema({
    _id: {
      type: ObjectId,
      auto: true,
    },
    user_id: {
      type: String,
      required: true,
    },
    category_id: {
      type: String,
      required: true,
    },
    url: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    date: {
      type: Number,
    },
    cover_url: {
      type: String,
    },
    estimated_time: {
      type: Number,
      required: true,
    },
    bookmark: {
      type: Boolean,
      required: true,
    },
    archived: {
      type: Boolean,
      required: true,
    },
    archived_at: {
      type: Number,
    },
  }, {
    // ref: https://mongoosejs.com/docs/guide.html#strict
    strict: 'throw',
  })

  ArticleSchema.index({ estimated_time: 1 })
  ArticleSchema.index({ user_id: 1, category_id: 1 })

  return mongoose.model('Article', ArticleSchema)
}
