// SchemaType
// https://mongoosejs.com/docs/schematypes.html#what-is-a-schematype

// SchemaType Options
// https://mongoosejs.com/docs/schematypes.html#schematype-options
//
module.exports = (app) => {
  const mongoose = app.mongoose
  const Schema = mongoose.Schema
  const { ObjectId } = Schema.Types

  const MessageSchema = new Schema({
    // Mongoose assigns an `id` virtual getter by default
    // which returns the document's _id field cast to a string
    // ref: https://mongoosejs.com/docs/guide.html#id
    _id: {
      type: ObjectId,
      auto: true,
    },
    user_id: {
      type: String,
      required: true,
    },
    type: {
      type: String, // message / article
      require: true,
    },
    article_id: {
      type: String,
    },
    content: {
      type: String,
      require: true,
    },
    send_to_pet: {
      type: Boolean,
      require: true,
    },
    sent_at: {
      type: Number,
    },
    updated_at: {
      type: Number,
    },
  }, {
    // ref: https://mongoosejs.com/docs/guide.html#strict
    strict: 'throw',
    // ref: https://mongoosejs.com/docs/guide.html#timestamps
    timestamps: {
      createdAt: 'sent_at',
      updatedAt: 'updated_at',
    },
  },
  )

  MessageSchema.index({ user_id: 1, sent_at: 1 })

  return mongoose.model('Message', MessageSchema)
}
