const admin = require('firebase-admin')
const serviceAccount = require('../../ss-team3-firebase-adminsdk-7xb8k-4b8754575e.json')
const e = require('../../lib/error')

// ref: https://firebase.google.com/docs/admin/setup
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
})

module.exports = (options, app) => {
  return async function auth (ctx, next) {
    const { request } = ctx
    const authHeader = request.header.authorization
    if (!authHeader) {
      throw new e.ErrorRes(e.NotAuthenticated)
    }
    const idToken = authHeader.slice(7)

    try {
      const decodedToken = await admin.auth().verifyIdToken(idToken)
      const { uid, name, email, picture } = decodedToken
      ctx.auth = { token: idToken, uid, name, email, picture }
    } catch (err) {
      console.error(err)
      throw new e.ErrorRes(e.JwtVerifyError)
    }

    await next()
  }
}
