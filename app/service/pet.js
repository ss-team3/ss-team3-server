const { Service } = require('egg')

class PetService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    Object.assign(params, { last_feed_at: Date.now() })
    const pet = await model.Pet.create(params)
    return pet
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Pet.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Pet.findById(_id).lean().exec()
    return data
  }

  // eslint-disable-next-line camelcase
  async findByUserId (user_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Pet.findOne({ user_id }).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.Pet.countDocuments(filter).lean().exec()
    const data = await model.Pet.find(filter, null, {
      limit,
      skip: offset,
      sort: order,
    }).lean().exec()
    return { total, data }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Pet.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Pet.updateOne({ _id }, params)
    return res
  }

  // eslint-disable-next-line camelcase
  async updateByUserId (user_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Pet.updateOne({ user_id }, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Pet.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Pet.deleteOne({ _id })
    return res
  }
}

module.exports = PetService
