const { Service } = require('egg')
const e = require('../../lib/error')

class ProductService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    const product = await model.Product.create(params)
    return product
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Product.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Product.findById(_id).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.Product.countDocuments(filter).lean().exec()
    const data = await model.Product.find(filter, null, {
      limit,
      skip: offset,
      sort: order,
    }).lean().exec()
    return { total, data }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Product.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Product.updateOne({ _id }, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Product.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Product.deleteOne({ _id })
    return res
  }

  // eslint-disable-next-line camelcase
  async buyOne ({ user_id, product_id }) {
    const { ctx } = this
    const { service } = ctx

    const { money } = await service.user.findByPK(user_id)
    const { price } = await service.product.findByPK(product_id)
    if (money - price < 0) {
      throw new e.ErrorRes(e.MoneyNotEnough, { money, price })
    }

    let res
    const ownedProduct = await service.ownedProduct.findByUserIdProductId(user_id, product_id)
    if (ownedProduct) {
      res = await service.ownedProduct.updateByPK(ownedProduct._id, { count: ownedProduct.count + 1 })
    } else {
      res = await service.ownedProduct.create({
        user_id,
        product_id,
        count: 1,
      })
    }
    service.user.updateByPK(user_id, { money: money - price })

    return res
  }
}

module.exports = ProductService
