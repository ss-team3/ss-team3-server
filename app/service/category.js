const { Service } = require('egg')
const e = require('../../lib/error')

class CategoryService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx

    if (['quick', 'short', 'long', 'focus', 'bookmark', 'All'].includes(params.name.toLowerCase())) {
      throw new e.ErrorRes(e.CategoryNameInvalid)
    }

    const category = await model.Category.create(params)
    return category
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Category.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Category.findById(_id).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.Category.countDocuments(filter).lean().exec()
    const data = await model.Category.find(filter, null, { limit, skip: offset, sort: order }).lean().exec()
    return { total, data }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Category.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Category.updateOne({ _id }, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Category.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Category.deleteOne({ _id })
    return res
  }

  async attachPosts (data, uid) {
    const { ctx } = this
    const { service } = ctx
    if (!Array.isArray(data) || data.some(datum => !datum._id)) {
      throw new Error('Can only attach data to Array object with `_id` field')
    }

    const categoryIds = data.map(datum => datum._id)
    const { data: posts } = await service.article.findAllPosts({ filter: { category_id: categoryIds, user_id: uid }, limit: null })
    for (const category of data) {
      category.articles = posts.filter(post => post.category_id === String(category._id))
    }
  }
}

module.exports = CategoryService
