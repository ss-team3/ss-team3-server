const { Service } = require('egg')

class UserService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    const user = await model.User.create(params)
    return user
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.User.findOne(filter).lean().exec()
    return data
  }

  async findByPK (uid) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.User.findOne({ uid }).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.User.countDocuments(filter).lean().exec()
    const data = await model.User.find(filter, null, { limit, skip: offset, sort: order }).lean().exec()
    return { total, data }
  }

  // updateOne(filter, params)

  async updateByPK (uid, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.User.updateOne({ uid }, params)
    return res
  }

  // deleteOne(filter)

  async deleteByPK (uid) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.User.deleteOne({ uid })
    return res
  }

  async attachWeekAlreadyRead (data) {
    const { ctx } = this
    const { service } = ctx
    if (!Array.isArray(data) || data.some(datum => !datum._id)) {
      throw new Error('Can only attach data to Array object with `_id` field')
    }

    const userIds = data.map(datum => datum.uid)
    const weekOffset = Date.now() - 7 * 24 * 60 * 60 * 1000
    const { data: articles } = await service.article.findAll({ filter: { user_id: userIds, archived_at: { $gte: weekOffset } }, limit: null })
    for (const user of data) {
      user.week_already_read = articles.filter(article => article.user_id === String(user.uid)).length
    }
  }
}

module.exports = UserService
