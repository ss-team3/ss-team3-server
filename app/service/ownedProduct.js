const { Service } = require('egg')
const e = require('../../lib/error')

class OwnedProductService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    const ownedProduct = await model.OwnedProduct.create(params)
    return ownedProduct
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.OwnedProduct.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.OwnedProduct.findById(_id).lean().exec()
    return data
  }

  // eslint-disable-next-line camelcase
  async findByUserIdProductId (user_id, product_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.OwnedProduct.findOne({ user_id, product_id }).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.OwnedProduct.countDocuments(filter).lean().exec()
    const data = await model.OwnedProduct.find(filter, null, {
      limit,
      skip: offset,
      sort: order,
    }).lean().exec()
    return { total, data }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.OwnedProduct.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.OwnedProduct.updateOne({ _id }, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.OwnedProduct.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.OwnedProduct.deleteOne({ _id })
    return res
  }

  // eslint-disable-next-line camelcase
  async useOne ({ ownedProduct_id, user_id }) {
    const { ctx } = this
    const { service } = ctx
    const [ownedProduct, pet] = await Promise.all([
      service.ownedProduct.findOne({ _id: ownedProduct_id, user_id }),
      service.pet.findByUserId(user_id),
    ])
    const product = await service.product.findByPK(ownedProduct.product_id)

    if (!ownedProduct) throw new e.ErrorRes(e.OwnedProductNotFound)
    if (ownedProduct.count <= 0) throw new e.ErrorRes(e.OwnedProductNotEnough)
    if (!product) throw new e.ErrorRes(e.ProductNotFound)
    if (!pet) throw new e.ErrorRes(e.PetNotFound)

    await Promise.all([
      service.ownedProduct.updateByPK(ownedProduct_id, { count: ownedProduct.count - 1 }),
      service.pet.updateByUserId(user_id, { hunger: Math.min(pet.hunger + product.energy, 12) }),
    ])

    return { success: true }
  }

  async attachProducts (data) {
    const { ctx } = this
    const { service } = ctx
    if (!Array.isArray(data) || data.some(datum => !datum._id)) {
      throw new Error('Can only attach data to Array object with `_id` field')
    }

    const productIds = data.map(datum => datum.product_id)
    const { data: products } = await service.product.findAll({ filter: { _id: productIds }, limit: null })
    const productObj = {}
    for (const product of products) {
      productObj[product._id] = product
    }

    for (const ownedProduct of data) {
      ownedProduct.product = productObj[ownedProduct.product_id]
    }
  }
}

module.exports = OwnedProductService
