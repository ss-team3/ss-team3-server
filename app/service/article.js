const { Service } = require('egg')

class ArticleService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    const article = await model.Article.create(params)
    return article
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Article.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Article.findById(_id).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.Article.countDocuments(filter).lean().exec()
    const data = await model.Article.find(filter, null, { limit, skip: offset, sort: order }).lean().exec()
    return { total, data }
  }

  async findAllPosts ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const projection = ['_id', 'user_id', 'category_id', 'url', 'title', 'cover_url', 'date', 'estimated_time', 'bookmark', 'archived', 'archived_at']
    const total = await model.Article.countDocuments(filter).lean().exec()
    const data = await model.Article.find(filter, projection, { limit, skip: offset, sort: order }).lean().exec()
    return { total, data }
  }

  async findRandomId ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const projection = '_id'
    const data = await model.Article.find(filter, projection, { limit, skip: offset, sort: order }).lean().exec()

    if (data.length) {
      return data[Math.floor(Math.random() * data.length)]._id
    } else {
      return null
    }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Article.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Article.updateOne({ _id }, params)
    return res
  }

  async updateMany (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Article.updateMany(filter, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Article.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Article.deleteOne({ _id })
    return res
  }

  async attachCategories (data, uid) {
    const { ctx } = this
    const { service } = ctx
    if (!Array.isArray(data) || data.some(datum => !datum._id)) {
      throw new Error('Can only attach data to Array object with `_id` field')
    }

    const categoryIds = [...new Set(data.map(datum => datum.category_id))]
    const { data: categories } = await service.category.findAll({ filter: { _id: categoryIds, user_id: uid }, limit: null })
    const categoryObj = {}
    for (const category of categories) {
      categoryObj[category._id] = category
    }

    for (const article of data) {
      article.category = categoryObj[article.category_id]
    }
  }
}

module.exports = ArticleService
