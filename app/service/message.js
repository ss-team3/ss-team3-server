const { Service } = require('egg')

class MessageService extends Service {
  async create (params) {
    const { ctx } = this
    const { model } = ctx
    const message = await model.Message.create(params)
    return message
  }

  async createCurrent (params) {
    const { ctx } = this
    const { service } = ctx
    const {
      // eslint-disable-next-line camelcase
      user_id,
      send_to_pet: sendToPet,
      content,
      type,
      article_filter: articleFilter,
    } = params

    let message
    if (type === 'message') {
      message = await service.message.create({
        user_id,
        type,
        content,
        send_to_pet: sendToPet,
      })
    } else if (type === 'article') {
      let randomArticleId = await service.article.findRandomId({ filter: { ...articleFilter, user_id } })
      if (randomArticleId) {
        message = await service.message.create({
          user_id,
          type,
          content,
          send_to_pet: sendToPet,
          article_id: randomArticleId,
        })
      } else {
        randomArticleId = await service.article.findRandomId({ filter: { user_id } })
        if (randomArticleId) {
          await service.message.create({
            user_id,
            type: 'message',
            content: '沒有這個長度的文章，這篇如何 0u0',
            send_to_pet: false,
          })
          message = await service.message.create({
            user_id,
            type,
            content,
            send_to_pet: sendToPet,
            article_id: randomArticleId,
          })
        } else {
          message = await service.message.create({
            user_id,
            type: 'message',
            content: '哇，你沒有文章可以看欸，快點去蒐集吧 QQ',
            send_to_pet: false,
          })
        }
      }
    }

    return message
  }

  async findOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Message.findOne(filter).lean().exec()
    return data
  }

  async findByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Message.findById(_id).lean().exec()
    return data
  }

  // eslint-disable-next-line camelcase
  async findByUserId (user_id) {
    const { ctx } = this
    const { model } = ctx
    const data = await model.Message.findOne({ user_id }).lean().exec()
    return data
  }

  async findAll ({ filter, limit, offset, order }) {
    const { ctx } = this
    const { model } = ctx
    const total = await model.Message.countDocuments(filter).lean().exec()
    const data = await model.Message.find(filter, null, {
      limit,
      skip: offset,
      sort: order,
    }).lean().exec()
    return { total, data }
  }

  async updateOne (filter, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Message.updateOne(filter, params)
    return res
  }

  async updateByPK (_id, params) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Message.updateOne({ _id }, params)
    return res
  }

  async deleteOne (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Message.deleteOne(filter)
    return res
  }

  async deleteByPK (_id) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Message.deleteOne({ _id })
    return res
  }

  async deleteMany (filter) {
    const { ctx } = this
    const { model } = ctx
    const res = await model.Message.deleteMany(filter)
    return res
  }

  async attachPosts (data, uid) {
    const { ctx } = this
    const { service } = ctx

    const articleIds = [...new Set(data.map(datum => datum.article_id))]
    const { data: articles } = await service.article.findAllPosts({ filter: { _id: articleIds, user_id: uid }, limit: null })
    const articleObj = {}
    for (const article of articles) {
      articleObj[article._id] = article
    }

    for (const message of data) {
      message.article = articleObj[message.article_id]
    }
  }
}

module.exports = MessageService
