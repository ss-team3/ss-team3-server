// egg-validate: https://github.com/eggjs/egg-validate
// parameter: https://github.com/node-modules/parameter#rule

const forbidden = {
  type: 'forbidden',
  required: false,
}

// eslint-disable-next-line camelcase
const created_at = forbidden
// eslint-disable-next-line camelcase
const updated_at = forbidden

const token = {
  type: 'string',
}

const _id = {
  type: 'string',
}

const filter = {
  type: 'object',
  required: false,
}
const limit = {
  type: 'int',
  min: 0,
  required: false,
}
const offset = {
  type: 'int',
  min: 0,
  required: false,
}
const order = {
  type: 'object',
  required: false,
}

const user = {
  uid: {
    type: 'string',
  },
  name: {
    type: 'string',
  },
  theme: {
    // TODO: use enum
    type: 'string',
  },
  font_family: {
    type: 'string',
  },
  font_size: {
    type: 'number',
  },
  money: {
    type: 'number',
    min: 0,
  },
  total_read_time: {
    type: 'number',
    min: 0,
  },
  total_read_article: {
    type: 'number',
    min: 0,
  },
  target_read_article: {
    type: 'int',
    min: 0,
  },
}
const article = {
  user_id: user.uid,
  category_id: _id,
  url: {
    type: 'string',
  },
  title: {
    type: 'string',
  },
  content: {
    type: 'string',
  },
  date: {
    type: 'number',
  },
  cover_url: {
    type: 'string',
  },
  estimated_time: {
    type: 'number',
  },
  bookmark: {
    type: 'boolean',
  },
  archived: {
    type: 'boolean',
  },
  archived_at: {
    type: 'number',
  },
}

const pet = {
  user_id: user.uid,
  name: {
    type: 'string',
  },
  type: {
    type: 'string',
  },
  hunger: {
    type: 'int',
    min: 0,
  },
  last_feed_at: {
    type: 'number',
  },
}

const product = {
  name: {
    type: 'string',
  },
  price: {
    type: 'int',
    min: 0,
  },
  energy: {
    type: 'int',
    min: 1,
  },
  type: {
    type: 'string',
  },
}

const ownedProduct = {
  user_id: user.uid,
  product_id: {
    type: 'string',
  },
  count: {
    type: 'int',
    min: 0,
  },
}

const category = {
  user_id: user.uid,
  name: {
    type: 'string',
  },
  avatar_url: {
    type: 'string',
  },
}

const message = {
  user_id: user.uid,
  type: {
    type: 'string',
  },
  article_id: {
    type: 'string',
  },
  content: {
    type: 'string',
  },
  send_to_pet: {
    type: 'boolean',
  },
}

module.exports = {
  token: { token },

  _id: { _id },
  uid: { uid: user.uid },
  filter: { filter },
  gets: {
    filter, limit, offset, order,
  },

  // Admin - user
  addUser: {
    uid: user.uid,
    name: user.name,
    theme: user.theme,
    font_family: user.font_family,
    font_size: user.font_size,
    money: forbidden,
    total_read_time: forbidden,
    total_read_article: forbidden,
    target_read_article: user.target_read_article,
    created_at,
    updated_at,
  },

  modifyUser: {
    uid: user.uid,
    name: {
      ...user.name,
      required: false,
    },
    theme: {
      ...user.theme,
      required: false,
    },
    font_family: {
      ...user.font_family,
      required: false,
    },
    font_size: {
      ...user.font_size,
      required: false,
    },
    money: {
      ...user.money,
      required: false,
    },
    total_read_time: {
      ...user.total_read_time,
      required: false,
    },
    total_read_article: {
      ...user.total_read_article,
      required: false,
    },
    target_read_article: {
      ...user.target_read_article,
      required: false,
    },
    created_at,
    updated_at,
  },

  // Api - user
  modifyCurrentUser: {
    _id: forbidden,
    uid: forbidden,
    name: {
      ...user.name,
      required: false,
    },
    theme: {
      ...user.theme,
      required: false,
    },
    font_family: {
      ...user.font_family,
      required: false,
    },
    font_size: {
      ...user.font_size,
      required: false,
    },
    money: {
      ...user.money,
      required: false,
    },
    total_read_time: {
      ...user.total_read_time,
      required: false,
    },
    total_read_article: forbidden,
    target_read_article: {
      ...user.target_read_article,
      required: false,
    },
    created_at,
    updated_at,
  },

  // Admin - Article
  addArticle: {
    _id: forbidden,
    user_id: user.uid,
    category_id: _id,
    url: article.url,
    title: forbidden,
    content: forbidden,
    date: forbidden,
    cover_url: forbidden,
    estimated_time: forbidden,
    /*
    title: {
      ...article.title,
      required: false,
    },
    content: {
      ...article.content,
      required: false,
    },
    date: {
      ...article.date,
      required: false,
    },
    cover_url: {
      ...article.cover_url,
      required: false,
    },
    estimated_time: {
      ...article.estimated_time,
      required: false,
    },
    */
    bookmark: {
      ...article.bookmark,
      required: false,
    },
    archived: {
      ...article.archived,
      required: false,
    },
    archived_at: forbidden,
    created_at,
    updated_at,
  },

  modifyArticle: {
    _id,
    user_id: forbidden,
    category_id: {
      ...article.category_id,
      required: false,
    },
    url: forbidden,
    title: forbidden,
    content: forbidden,
    date: forbidden,
    cover_url: forbidden,
    estimated_time: {
      ...article.estimated_time,
      required: false,
    },
    bookmark: {
      ...article.bookmark,
      required: false,
    },
    archived: {
      ...article.archived,
      required: false,
    },
    archived_at: {
      ...article.archived_at,
      required: false,
    },
    created_at,
    updated_at,
  },

  // Api - Article
  addCurrentArticle: {
    _id: forbidden,
    user_id: forbidden,
    category_id: _id,
    url: article.url,
    title: forbidden,
    content: forbidden,
    date: forbidden,
    cover_url: forbidden,
    estimated_time: forbidden,
    bookmark: {
      ...article.bookmark,
      required: false,
    },
    /*
    title: {
      ...article.title,
      required: false,
    },
    content: {
      ...article.content,
      required: false,
    },
    date: {
      ...article.date,
      required: false,
    },
    cover_url: {
      ...article.cover_url,
      required: false,
    },
    estimated_time: {
      ...article.estimated_time,
      required: false,
    },
    */
    archived: forbidden,
    archived_at: forbidden,
    created_at,
    updated_at,
  },

  modifyCurrentArticle: {
    _id: forbidden,
    user_id: forbidden,
    category_id: {
      ...article.category_id,
      required: false,
    },
    url: forbidden,
    title: forbidden,
    content: forbidden,
    date: forbidden,
    cover_url: forbidden,
    estimated_time: forbidden,
    bookmark: {
      ...article.bookmark,
      required: false,
    },
    archived: {
      ...article.archived,
      required: false,
    },
    archived_at: forbidden,
    created_at,
    updated_at,
  },

  // Admin - Pet
  addPet: {
    _id: forbidden,
    user_id: user.uid,
    name: pet.name,
    type: pet.type,
    hunger: pet.hunger,
    last_feed_at: forbidden,
    created_at,
    updated_at,
  },

  modifyPet: {
    _id,
    user_id: {
      ...user.uid,
      required: false,
    },
    name: {
      ...pet.name,
      required: false,
    },
    type: {
      ...pet.type,
      required: false,
    },
    hunger: {
      ...pet.hunger,
      required: false,
    },
    last_feed_at: {
      ...pet.last_feed_at,
      required: false,
    },
    created_at,
    updated_at,
  },

  // Api - Pet
  modifyCurrentPet: {
    _id: forbidden,
    user_id: forbidden,
    name: {
      ...pet.name,
      required: false,
    },
    type: forbidden,
    hunger: forbidden,
    last_feed_at: forbidden,
    created_at,
    updated_at,
  },

  // Admin - Product
  addProduct: {
    _id: forbidden,
    name: product.name,
    price: product.price,
    energy: product.energy,
    type: product.type,
  },

  modifyProduct: {
    _id: forbidden,
    name: {
      ...product.name,
      required: false,
    },
    price: {
      ...product.price,
      required: false,
    },
    energy: {
      ...product.energy,
      required: false,
    },
    type: {
      ...product.type,
      required: false,
    },
  },

  // Admin - OwnedProduct
  addOwnedProduct: {
    _id: forbidden,
    user_id: ownedProduct.user_id,
    product_id: ownedProduct.product_id,
    count: ownedProduct.count,
  },

  modifyOwnedProduct: {
    _id,
    user_id: forbidden,
    product_id: forbidden,
    count: ownedProduct.count,
  },

  // Admin - Category
  addCategory: {
    _id: forbidden,
    user_id: user.uid,
    name: category.name,
    avatar_url: {
      ...category.avatar_url,
      required: false,
    },
    created_at,
    updated_at,
  },

  modifyCategory: {
    _id,
    user_id: {
      ...user.uid,
      required: false,
    },
    name: {
      ...category.name,
      required: false,
    },
    avatar_url: {
      ...category.avatar_url,
      required: false,
    },
    created_at,
    updated_at,
  },

  // Api - Category
  addCurrentCategory: {
    _id: forbidden,
    user_id: forbidden,
    name: {
      ...category.name,
      required: false,
    },
    avatar_url: {
      ...category.avatar_url,
      required: false,
    },
    created_at,
    updated_at,
  },

  getCurrentCategory: {
    _id: forbidden,
    user_id: forbidden,
    name: { ...category.name },
    avatar_url: forbidden,
    created_at,
    updated_at,
  },

  modifyCurrentCategory: {
    _id: forbidden,
    user_id: forbidden,
    name: {
      ...category.name,
      required: false,
    },
    avatar_url: {
      ...category.avatar_url,
      required: false,
    },
    created_at,
    updated_at,
  },

  // admin - Message
  addMessage: {
    _id: forbidden,
    user_id: message.user_id,
    type: message.type,
    article_id: {
      ...message.article_id,
      required: false,
    },
    content: {
      ...message.content,
      required: false,
    },
    send_to_pet: message.send_to_pet,
    sent_at: forbidden,
    updated_at,
  },

  modifyMessage: {
    _id,
    user_id: {
      ...message.user_id,
      required: false,
    },
    type: {
      ...message.type,
      required: false,
    },
    article: {
      ...message.article_id,
      required: false,
    },
    content: {
      ...message.content,
      required: false,
    },
    send_to_pet: {
      ...message.send_to_pet,
      required: false,
    },
    sent_at: forbidden,
    updated_at,
  },

  // Api - Message
  addCurrentMessage: {
    article_filter: filter,
    type: {
      ...message.type,
      required: true,
    },
    content: {
      ...message.content,
      required: false,
    },
    send_to_pet: {
      ...message.send_to_pet,
      required: true,
    },
  },
}
