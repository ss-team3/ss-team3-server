const codes = {
  // NotAuthenticated - User are not carrying JWT to access the API
  NotAuthenticated: 1000,
  // NotAuthorized - User are not allowed to access the API
  NotAuthorized: 1001,
  // JwtVerifyError - Failed to verify JWT, invalid token
  JwtVerifyError: 1002,
  // JwtSignError - Failed to sign JWT
  JwtSignError: 2000,
  // UserNotFound - Failed to find user in database
  UserNotFound: 3000,
  // CategoryNotFound - Failed to find category in database
  CategoryNotFound: 4000,
  // CategoryNameInvalid - Category's name is invalid. Such as 'quick', 'short', 'long', 'focus'.
  CategoryNameInvalid: 4001,
  // DefaultCategoryNotFound - Failed to find default category in database
  DefaultCategoryNotFound: 4002,
  // ArticleNotFound - Failed to find article in database
  ArticleNotFound: 5000,
  // ArticleExtractionFail - Failed to extract article
  ArticleExtractionFail: 5001,
  // PetNotFound - Failed to find pet in database,
  PetNotFound: 6000,
  // ProductNotFound - Failed to find product in database,
  ProductNotFound: 7000,
  // ProductNotFound - Failed to find product in database,
  MoneyNotEnough: 7001,
  // OwnedProductNotFound - Failed to fund ownedProduct in database,
  OwnedProductNotFound: 8000,
  // OwnedProductNotEnough - Failed to use ownedProduct, since does not have enough ownedProduct.
  OwnedProductNotEnough: 8001,
}

const code2msg = {
  [codes.NotAuthenticated]: 'User are not authenticated to access the API.',
  [codes.NotAuthorized]: 'User are not authorized to access the API.',
  [codes.JwtVerifyError]: 'Failed to verify JWT, invalid token.',
  [codes.JwtSignError]: 'Failed to sign JWT.',

  [codes.UserNotFound]: 'Failed to find user in database.',

  [codes.CategoryNotFound]: 'Failed to find category in database.',
  [codes.CategoryNameInvalid]: "Category's name is invalid. Such as 'quick', 'short', 'long', 'focus'.",
  [codes.DefaultCategoryNotFound]: 'Failed to find default category in database',

  [codes.ArticleNotFound]: 'Failed to find article in database.',
  [codes.ArticleExtractionFail]: 'Failed to extract article.',

  [codes.PetNotFound]: 'Failed to find pet in database.',

  [codes.ProductNotFound]: 'Failed to find product in database.',
  [codes.MoneyNotEnough]: 'Failed to buy product, since user does not have enough money.',

  [codes.OwnedProductNotFound]: 'Failed to find ownedProduct in database.',
  [codes.OwnedProductNotEnough]: 'Failed to use ownedProduct, since does not have enough ownedProduct.',
}

// Custom Error Class
class ErrorRes extends Error {
  constructor (code, data, status = 400) {
    const msg = code2msg[code] || 'Error occurred!'
    super(msg)
    this.code = code
    this.msg = msg
    this.data = data
    this.status = status
  }
}

module.exports = {
  ...codes,
  ErrorRes,
}
