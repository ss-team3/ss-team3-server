const { isValidObjectId } = require('mongoose')

module.exports = app => {
  // validation rule: objectId
  app.validator.addRule('objectId', (rule, value) => {
    if (!isValidObjectId(value)) {
      return 'invalid objectId'
    }
  })
  // validation rule: forbidden
  app.validator.addRule('forbidden', (rule, value) => {
    if (value !== undefined) { return 'forbidden field' }
  })
}
