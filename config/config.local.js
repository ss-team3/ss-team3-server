module.exports = {
  keys: 'Bwn8iQ1lfo',
  logger: {
    level: 'NONE',
  },
  security: {
    csrf: false,
  },
  cors: {
    origin: '*',
    allowMethods: 'POST',
  },
  mongoose: {
    url: 'mongodb://127.0.0.1:27017',
    options: {
      dbName: 'ss_team3',
      user: 'root',
      pass: 'password',
    },
  },
}
