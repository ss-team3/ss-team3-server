module.exports = {
  keys: 'yhXjrhyx9E',
  logger: {
    level: 'NONE',
    // console logger at production mode (not recommend for performance concern)
    // ref: https://eggjs.org/en/core/logger.html#in-terminal
    disableConsoleAfterReady: false,
  },
  security: {
    // TODO: fix me
    csrf: false,
  },
  cors: {
    origin: '*',
    allowMethods: 'POST',
  },
  mongoose: {
    // ref: https://mongoosejs.com/docs/connections.html#options
    url: 'mongodb+srv://ss-team3.cespb.mongodb.net',
    options: {
      dbName: 'ss_team3',
      user: 'ss-team3-admin',
      pass: 'cpH8jXXKD473ra9',
      useUnifiedTopology: true,
    },
  },
}
