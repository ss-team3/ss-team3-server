module.exports = {
  onerror: {
    all (_err, ctx) {
      const { msg, code, data, status } = _err
      // stringify Error object to JSON format to prevent 'ERR_INVALID_ARG_TYPE` error
      ctx.body = JSON.stringify({ msg, code, data, status })
      ctx.set('content-type', 'application/json')
      ctx.status = status
    },
  },
}
